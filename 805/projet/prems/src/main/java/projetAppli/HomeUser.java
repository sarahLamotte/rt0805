package projetAppli;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.eclipse.jetty.http.MetaData.Response;

import Mobile.Activity;
    
public class HomeUser extends HttpServlet{
    private static final String CHAMP_PSEUDO  = "pseudo";
    private static final String CHAMP_PASS   = "pass";
    private static final String CHAMP_TYPEFORM = "typeF";
    public static Utilisateur test = null;

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
        {
            response.setContentType("text/html");
            Utilisateur test = new Utilisateur();
            String pseudo = getValeurChamp( request, CHAMP_PSEUDO );
            String motDePasse = getValeurChamp(request, CHAMP_PASS);
            String typeForm = getValeurChamp(request, CHAMP_TYPEFORM);
            response.setStatus(HttpServletResponse.SC_OK);
            PrintWriter out = response.getWriter();
            Activity gn = null;
            switch(typeForm){
                case "Connexion":
                motDePasse = getValeurChamp( request, CHAMP_PASS );
                test = getUser(pseudo);
                System.out.println("case 1 mdp : "+ motDePasse);
                System.out.println("case 1 user mdp:"+test.getMotDePasse());
                if(motDePasse.equals(test.getMotDePasse())){
                    out.println("<h1>Bienvenue " +test.getPseudo() + "!</h1>");
                   if((test.getActivities() != null)){
                    out.println("Dernière activité enregistrée : <br>");
                    gn = test.getActivities().get(test.getActivities().size()-1);
                    out.println("Type d'activité : " + gn.getType() + "<br> Coordonnées : <br>" + gn.getCoords() + "Heure de début:" + gn.getHD() +"<br> Heure de fin :"+ gn.getHF());
                    out.println("<form method='get' action=activite></label><input type='hidden' id='pseudo' name='pseudo' value="+test.getPseudo()+"><input type='submit' value='Suivre une activite' name='typeF' id='typeF' /></form>");           
                    out.println("<form method='get' action=historique></label><input type='hidden' id='pseudo' name='pseudo' value="+test.getPseudo()+"><input type='submit' value='Voir l historique des activités' /></form>");   
                    out.println("<form method='get' action=statistiques></label><input type='hidden' id='pseudo' name='pseudo' value="+test.getPseudo()+"><input type='submit' value='Voir les statistiques' /></form>");    
                    out.println("<form method='get' action=accueil></label><input type='submit' value='Déconnexion' /></form>");     
                    
                }
                    out.println("<form method='get' action=accueil></label><input type='submit' value='Déconnexion' /></form>");     
                
                    }
                else
                {
                    out.println("Mauvais mot de passe <br>");
                    out.println("<form method='get' action=accueil><input type='submit' value='Retour' /></form>");           
                    
                }
                break;
                case "Inscription":
                motDePasse = getValeurChamp( request, CHAMP_PASS );
                System.out.println(motDePasse);
                test.setPseudo(pseudo);
                test.setMotDePasse(motDePasse);
                inscrire(test);
                out.println("<h1>Bienvenue " +test.getPseudo() + "!</h1>");
                out.println("<form method='get' action=activite></label><input type='hidden' id='pseudo' name='pseudo' value="+test.getPseudo()+"><input type='submit' value='Suivre une activité' name='typeF' id='typeF' /></form>");           
                out.println("<form method='get' action=historique></label><input type='hidden' id='pseudo' name='pseudo' value="+test.getPseudo()+"><input type='submit' value='Voir l historique des activités' /></form>");   
                out.println("<form method='get' action=statistiques></label><input type='hidden' id='pseudo' name='pseudo' value="+test.getPseudo()+"><input type='submit' value='Voir les statistiques' /></form>");    
                out.println("<form method='get' action=accueil></label><input type='submit' value='Déconnexion' /></form>");     
                
                break;
                case "Retour":
                test = getUser(pseudo);
                out.println("Dernière activité enregistrée : <br>");
                gn = test.getActivities().get(test.getActivities().size()-1);
                out.println("Type d'activité : " + gn.getType() + "<br> Coordonnées : <br>" + gn.getCoords() + "<br>Heure de début:" + gn.getHD() +"<br> Heure de fin :"+ gn.getHF());
                out.println("<form method='get' action=activite></label><input type='hidden' id='pseudo' name='pseudo' value="+test.getPseudo()+"><input type='submit' value='Suivre une activité' name='typeF' id='typeF' /></form>");           
                out.println("<form method='get' action=historique></label><input type='hidden' id='pseudo' name='pseudo' value="+test.getPseudo()+"><input type='submit' value='Voir l historique des activités' /></form>");
                out.println("<form method='get' action=statistiques></label><input type='hidden' id='pseudo' name='pseudo' value="+test.getPseudo()+"><input type='submit' value='Voir les statistiques' /></form>");    
                out.println("<form method='get' action=accueil></label><input type='submit' value='Déconnexion' /></form>");     
                
                break;
                default:
                break;
            }
            }

        private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
            String valeur = request.getParameter( nomChamp );
            if ( valeur == null || valeur.trim().length() == 0 ) {
                return null;
            } else {
                return valeur;
            }
        }
        public static double calcDistance(String a, String b) {
            double lat1 = Double.parseDouble(a.substring(0, a.indexOf(",")));
            double lat2 = Double.parseDouble(b.substring(0, b.indexOf(",")));
            double lon1 = Double.parseDouble(a.substring(a.indexOf(",") + 1));
            double lon2 = Double.parseDouble(b.substring(b.indexOf(",") + 1));
            double theta = lon1 - lon2;
            double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
                    + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
            dist = Math.acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            return (dist);
        }
    
        private static double deg2rad(double deg) {
            return (deg * Math.PI / 180.0);
        }
    
        private static double rad2deg(double rad) {
            return (rad * 180.0 / Math.PI);
        }
    
        public void inscrire(Utilisateur user) {

            ObjectMapper mapper = new ObjectMapper();
    
            try {
    
                String filename= "./projet/prems/src/main/data/"+user.getPseudo()+".json";
                FileWriter fw = new FileWriter(filename,true); //the true will append the new data
                
                // Java objects to JSON string - compact-print
                String jsonString = mapper.writeValueAsString(user);
    
                System.out.println(jsonString);
    
                // Java objects to JSON string - pretty-print
                String jsonInString2 = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);
    
                System.out.println(jsonInString2);
                fw.write(jsonString);//appends the string to the file
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    
        }

        public static Utilisateur getUser(String pseudo) throws IOException {

            File fr = new File("./projet/prems/src/main/data/" + pseudo + ".json");
            Utilisateur user = new Utilisateur();
            ArrayList<Activity> activites = new ArrayList<Activity>();
             
            BufferedReader br = new BufferedReader(new FileReader(fr));
            user.setPseudo(pseudo);
            String st, mdp,t,type,hd,hf,lastCoord,coords;
            while ((st = br.readLine()) != null) {
                System.out.println(st);
                if (st.contains("motDePasse")) {
                    String tmp = st.substring(st.indexOf("motDePasse") + 13);
                    mdp = (tmp.substring(0, tmp.indexOf('"')));
                    //System.out.println(mdp);
                    user.setMotDePasse(mdp);
                }
                if (st.contains("type")) {
                   
                    String strFind = "type";
                    int count = 0, fromIndex = 0;
                    String tmp = st.substring(st.indexOf("activities") + 13, st.indexOf("pseudo"));
                    while ((fromIndex = tmp.indexOf(strFind, fromIndex)) != -1) {
                        //System.out.println("Found at index: " + fromIndex);
                        count++;
                        fromIndex++;
                        ArrayList<String> c = new ArrayList<String>();
                        t = tmp.substring(fromIndex-1);
                        type = t.substring(t.indexOf("type") + 7, t.indexOf(",")-1);
                        hd = t.substring(t.indexOf("hd")+5, t.indexOf(",",t.indexOf("hd"))-1);
                        hf = t.substring(t.indexOf("hf") + 5, t.indexOf(",",t.indexOf("hf"))-1);
                        lastCoord = t.substring(t.indexOf("lastCoord")+12,t.indexOf("coord")-3);
                        coords = t.substring(t.indexOf("coords")+8,t.indexOf("}"));
                        Activity n = new Activity(user.getPseudo());
                        n.setType(type);
                        n.setHD(hd);
                        n.setHF(hf);
                        n.setlastCoord(lastCoord);
                        int fi=0;
                        String si="[",tmp2;
                        while ((fi = coords.indexOf(si, fi)) != -1) {
                            count++;
                            fi++;
                            tmp2 = coords.substring(fi+1,coords.indexOf("]", fi)-1);
                            //System.out.println(tmp2);
                            c.add(tmp2);
                        }
                        n.setCoords(c); 
                        activites.add(n);
                    }
                }
            }
            user.setActivites(activites);
            return user;
        }

        public static void main(String[] args) throws IOException {
               
        }

}