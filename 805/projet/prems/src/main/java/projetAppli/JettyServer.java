package projetAppli;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
 
public class JettyServer extends AbstractHandler
{
    public void handle(String target,
                       Request baseRequest,
                       HttpServletRequest request,
                       HttpServletResponse response) 
        throws IOException, ServletException
    {
        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);
        response.getWriter().println("<h1>Hello World</h1>");
    }
 
    public static void main(String[] args) throws Exception
    {
        Server server = new Server(8080);
        server.setHandler(new JettyServer());
        ServletHandler handler = new ServletHandler();
        handler.addServletWithMapping(Accueil.class, "/accueil");
        handler.addServletWithMapping(HomeUser.class, "/homeUser");
        handler.addServletWithMapping(Connexion.class, "/connexion");
        handler.addServletWithMapping(ActivitePage.class, "/activite");
        handler.addServletWithMapping(Inscription.class, "/inscription");
        handler.addServletWithMapping(History.class, "/historique");
        handler.addServletWithMapping(Statistiques.class, "/statistiques");

        server.setHandler(handler);
 
        server.start();
        server.join();
    }
}