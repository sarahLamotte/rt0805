package projetAppli;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import Mobile.Activity;
import Mobile.listeActivites;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Statistiques extends HttpServlet {

    private static final String CHAMP_PSEUDO = "pseudo";

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException
    {   
        String pseudo = getValeurChamp(req, CHAMP_PSEUDO);
        System.out.println(pseudo);
        Utilisateur test = getUser(pseudo);
        System.out.println(test.getPseudo());
        resp.setContentType("text/html");
        resp.setStatus(HttpServletResponse.SC_OK);
        PrintWriter out = resp.getWriter();
        double km = 0;
        String tmp="", tmp1;
        String res;
        Map<listeActivites,Integer> moyenneTemps = new HashMap<listeActivites,Integer>();
        Map<listeActivites,String> tempsTotal = new HashMap<listeActivites,String>();
        out.println("<h1>Statistiques</h1>");
        for (int i=0; i < test.getActivities().size(); i++){
            listeActivites type = test.getActivities().get(i).getType();
            if(tempsTotal.get(type)!=null){
                tempsTotal.put(type, ajoutDuree(tempsTotal.get(type), test.getActivities().get(i).duree()));
            }
            else{
                tempsTotal.put(type, test.getActivities().get(i).duree());
            }

        }
        Integer cpt=0;
        for (int i=0; i < test.getActivities().size(); i++){
            listeActivites type = test.getActivities().get(i).getType();
            if(moyenneTemps.get(type)!=null){
                cpt=moyenneTemps.get(type)+1;
                moyenneTemps.put(type, cpt);
            }
            else{
                moyenneTemps.put(type, Integer.parseInt("1"));
            }
        }

        out.println("<table><thead><tr><th>Satistiques de vos activités</th></tr></thead><tbody><tr>");
        out.println("<td>Activité</td>");
        Set cles = tempsTotal.keySet();
        Iterator it = cles.iterator();
        while (it.hasNext()){
            Object cle = it.next(); 
            Object valeur = tempsTotal.get(cle); 

            out.println("<td>" +cle +"</td>");
        }
        out.println("</tr><tr><td>Temps total</td>");
        cles = tempsTotal.keySet();
        it = cles.iterator();
        while (it.hasNext()){
            Object cle = it.next(); 
            Object valeur = tempsTotal.get(cle); 
            out.println("<td>" +valeur+"</td>");
        }
        out.println("</tr><tr><td>Temps en moyenne</td>");
        cles = moyenneTemps.keySet();
        it = cles.iterator();
        while (it.hasNext()){
            Object cle = it.next(); 
            Integer valeur = moyenneTemps.get(cle); 
            out.println("<td>" +tempsMoyen(tempsTotal.get(cle), valeur)+"</td>");
        }
        out.println("</tr></tbody></table>");
        //out.println("Type d'activité : " + test.getActivities().get(i).getType() + "<br> Coordonnées : <br>" + test.getActivities().get(i).getCoords() + "<br>Heure de début:" + test.getActivities().get(i).getHD() +"<br> Heure de fin :"+test.getActivities().get(i).getHF()+"<br><br>");
        

        out.println("<br><form method='get' action=homeUser> <input type='hidden' id='pseudo' name='pseudo' value="+test.getPseudo()+"><input type='submit' value='Retour' name='typeF' id='typeF'/></form>");
            
    }

    public static String ajoutDuree(String h1, String h2) {
        if(h2.length()>1){
            return h1;
        }
        int Hdeb = Integer.parseInt(h1.substring(0, h1.indexOf(":", 0)));
        int Mdeb = Integer.parseInt(h1.substring(h1.indexOf(":", 0) + 1));
        int Hfin = Integer.parseInt(h2.substring(0, h2.indexOf(":", 0)));
        int Mfin = Integer.parseInt(h2.substring(h2.indexOf(":", 0) + 1));
        int resH = (Hfin + Hdeb);
        int resM = (Mdeb + Mfin);
        int tmpM = 0, tmpH = 0;
        if (resM >= 60) {
            tmpM = resM % 60;
            tmpH = resM / 60;
        }
        resH += tmpH;
        return resH + ":" + resM;
    }

    public static String tempsMoyen(String h1, Integer div) {
        int Hdeb = Integer.parseInt(h1.substring(0, h1.indexOf(":", 0)));
        int Mdeb = Integer.parseInt(h1.substring(h1.indexOf(":", 0) + 1));
        int tmpH = 0,tmpM;
        int totMin = Hdeb*60 + Mdeb;
        System.out.println(totMin);
        int resM = totMin/div;
        int minM = totMin%div;
        System.out.println(resM + "dd"+minM);
        if (resM >= 60) {
            tmpM = resM % 60 + minM;
            tmpH = resM / 60;
        }
        else{
            tmpM=resM;
        }
        return tmpH +":"+tmpM;
    }

    private static String getValeurChamp(HttpServletRequest request, String nomChamp) {
        String valeur = request.getParameter(nomChamp);
        if (valeur == null || valeur.trim().length() == 0) {
            return null;
        } else {
            return valeur;
        }
    }

    public static Utilisateur getUser(String pseudo) throws IOException {

        File fr = new File("./projet/prems/src/main/data/" + pseudo + ".json");
        Utilisateur user = new Utilisateur();
        ArrayList<Activity> activites = new ArrayList<Activity>();

        BufferedReader br = new BufferedReader(new FileReader(fr));
        user.setPseudo(pseudo);
        String st, mdp, t, type, hd, hf, lastCoord, coords;
        while ((st = br.readLine()) != null) {
            System.out.println(st);
            if (st.contains("motDePasse")) {
                String tmp = st.substring(st.indexOf("motDePasse") + 13);
                mdp = (tmp.substring(0, tmp.indexOf('"')));
                // System.out.println(mdp);
                user.setMotDePasse(mdp);
            }
            if (st.contains("type")) {

                String strFind = "type";
                int count = 0, fromIndex = 0;
                String tmp = st.substring(st.indexOf("activities") + 13, st.indexOf("pseudo"));
                while ((fromIndex = tmp.indexOf(strFind, fromIndex)) != -1) {
                    // System.out.println("Found at index: " + fromIndex);
                    fromIndex++;
                    ArrayList<String> c = new ArrayList<String>();
                    t = tmp.substring(fromIndex - 1);
                    type = t.substring(t.indexOf("type") + 7, t.indexOf(",") - 1);
                    hd = t.substring(t.indexOf("hd") + 5, t.indexOf(",", t.indexOf("hd")) - 1);
                    hf = t.substring(t.indexOf("hf") + 5, t.indexOf(",", t.indexOf("hf")) - 1);
                    lastCoord = t.substring(t.indexOf("lastCoord") + 12, t.indexOf("coord") - 3);
                    coords = t.substring(t.indexOf("coords") + 8, t.indexOf("}"));
                    Activity n = new Activity(user.getPseudo());
                    n.setType(type);
                    n.setHD(hd);
                    n.setHF(hf);
                    n.setlastCoord(lastCoord);
                    int fi = 0, dep = 0;
                    String si = "\"", tmp2, tmp3 = "";
                    while ((fi = coords.indexOf(si, fi)) != -1) {
                        count++;
                        t = coords.substring(fi + 1);
                        System.out.println(coords);

                        if (t.contains("\"")) {
                            tmp2 = t.substring(0, t.indexOf("\""));
                            System.out.println("coordonnées recup :" + tmp2);
                            if (!tmp2.equals(",")) {
                                System.out.println("true");
                                c.add(tmp2);
                            }
                        }
                        fi++;
                    }
                    n.setCoords(c);
                    activites.add(n);
                }
            }
        }
        user.setActivites(activites);
        return user;
    }

    public static void main(String[] args) throws IOException {
        Activity n = new Activity("test");
        System.out.println(n.getCoords());
        System.out.println(n.getHF());
        n.addNewCoords(n.getCoords());
        n.addNewCoords(n.getCoords());
        n.addNewCoords(n.getCoords());
        n.addNewCoords(n.getCoords());
        n.addNewCoords(n.getCoords());
        System.out.println(n.getCoords());
        System.out.println(n.getHF());
        System.out.println("Duree totale :" + n.duree());
        System.out.println(tempsMoyen(n.duree(), 2));
    }
}