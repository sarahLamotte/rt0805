package projetAppli;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.databind.ObjectMapper;

import Mobile.Activity;

public class Utilisateur {
	public ArrayList<Activity> activities;
	public String pseudo;
	public String motDePasse;

	Utilisateur(){
		this.activities = new ArrayList<Activity>();
	}

	public String getPseudo() {
		return this.pseudo;
	}

	public String getMotDePasse() {
		return this.motDePasse;
	}

	public void setPseudo(String pseudo) {
		this.pseudo=pseudo;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse=motDePasse;
	}

	public void setActivites(ArrayList<Activity> act){
		this.activities=act;
	}

	public void addActivity(Activity gn){
		this.activities.add(gn);
	}
	
	public static void main(String[] args){
		Utilisateur test = new Utilisateur();
		test.setPseudo("ttt");
		test.setMotDePasse("motDePasse");
		System.out.println(test.getPseudo());
		System.out.println(test.getMotDePasse());
		Activity a1 = new Activity(test.getPseudo());
		test.addActivity(a1);
		System.out.println(test.getActivities());
		Activity a2 = new Activity(test.getPseudo());
		test.addActivity(a2);
		System.out.println(test.getActivities());
		Activity a3 = new Activity(test.getPseudo());
		test.addActivity(a3);
		System.out.println(test.getActivities());
    }

	public ArrayList<Activity> getActivities() {
		return this.activities;
	}

}