package projetAppli;

import Mobile.Activity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.taglibs.standard.tag.common.core.Util;

public class ActivitePage extends HttpServlet{

    private static final String CHAMP_PSEUDO = "pseudo";
    private static String pseudo = null;
    private static final String CHAMP_TYPEFORM = "typeF";
    

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException
    {   
        resp.setContentType("text/html");
        resp.setStatus(HttpServletResponse.SC_OK);
        PrintWriter out = resp.getWriter();
        pseudo = getValeurChamp(req, CHAMP_PSEUDO);
        String tF = getValeurChamp(req, CHAMP_TYPEFORM);
        Utilisateur test = getUser(pseudo);
        Activity gn;
        System.out.println(tF);
         if(tF.equals("Actualiser")){
                gn = test.getActivities().get(test.getActivities().size()-1);
                out.println("Type d'activité : " + gn.getType() + "<br> Coordonnées : <br>" + gn.getCoords() + "<br>Heure de début:" + gn.getHD() );
                gn.addNewCoords(gn.getCoords());    
                out.println("<br>Heure de fin: " + gn.getHF() + "<br>");        
                double km=0;
                int x = test.getActivities().size()-1;
                for(int j=1; j < test.getActivities().get(x).getCoords().size();j++){
                    km += calcDistance(test.getActivities().get(x).getCoords().get(j-1),test.getActivities().get(x).getCoords().get(j));
                }
                out.println("<br>");
                out.println("Nombre de km parcourus : " + km + "<br>");
                out.println(" <br> Nouvelles coordonnées : " + gn.getLastCoord() + "<br>");  
                out.println("<form method='get' action=homeUser> <input type='hidden' id='pseudo' name='pseudo' value="+test.getPseudo()+"><input type='submit' value='Retour' name='typeF' id='typeF'/></form>");
                out.println("<form method='get' action=activite> <input type='hidden' id='pseudo' name='pseudo' value="+test.getPseudo()+"><input type='submit' value='Actualiser' name='typeF' id='typeF'/></form>");
        }
        else{
            gn = new Activity(test.getPseudo());
            test.addActivity(gn);
            out.println("<h1>Dernière activité</h1>");
            out.println("Type d'activité : " + gn.getType() + "<br> Coordonnées : <br>" + gn.getLastCoord() + "<br>Heure de début:" + gn.getHD() +"<br> Heure de fin :"+ gn.getHF());
            out.println("<form method='get' action=homeUser> <input type='hidden' id='pseudo' name='pseudo' value="+test.getPseudo()+"><input type='submit' value='Retour' name='typeF' id='typeF'/></form>");
            out.println("<form method='get' action=activite> <input type='hidden' id='pseudo' name='pseudo' value="+test.getPseudo()+"><input type='submit' value='Actualiser' name='typeF' id='typeF'/></form>");
        }
        enregistrerActivite(test);
    }

    private static void enregistrerActivite(Utilisateur user) {
        ObjectMapper mapper = new ObjectMapper();
        try {

            // Java objects to JSON file
            mapper.writeValue(new File("./projet/prems/src/main/data/" + user.getPseudo() + ".json"), user);

            // Java objects to JSON string - compact-print
            String jsonString = mapper.writeValueAsString(user);

            //System.out.println(jsonString);

            // Java objects to JSON string - pretty-print
            String jsonInString2 = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);

            //System.out.println(jsonInString2);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static String getValeurChamp(HttpServletRequest request, String nomChamp) {
        String valeur = request.getParameter(nomChamp);
        if (valeur == null || valeur.trim().length() == 0) {
            return null;
        } else {
            return valeur;
        }
    }

    public static Utilisateur getUser(String pseudo) throws IOException {

        File fr = new File("./projet/prems/src/main/data/" + pseudo + ".json");
        Utilisateur user = new Utilisateur();
        ArrayList<Activity> activites = new ArrayList<Activity>();
         
        BufferedReader br = new BufferedReader(new FileReader(fr));
        user.setPseudo(pseudo);
        String st, mdp,t,type,hd,hf,lastCoord,coords;
        while ((st = br.readLine()) != null) {
            System.out.println(st);
            if (st.contains("motDePasse")) {
                String tmp = st.substring(st.indexOf("motDePasse") + 13);
                mdp = (tmp.substring(0, tmp.indexOf('"')));
                //System.out.println(mdp);
                user.setMotDePasse(mdp);
            }
            if (st.contains("type")) {
               
                String strFind = "type";
                int count = 0, fromIndex = 0;
                String tmp = st.substring(st.indexOf("activities") + 13, st.indexOf("pseudo"));
                while ((fromIndex = tmp.indexOf(strFind, fromIndex)) != -1) {
                    //System.out.println("Found at index: " + fromIndex);
                    fromIndex++;
                    ArrayList<String> c = new ArrayList<String>();
                    t = tmp.substring(fromIndex-1);
                    type = t.substring(t.indexOf("type") + 7, t.indexOf(",")-1);
                    hd = t.substring(t.indexOf("hd")+5, t.indexOf(",",t.indexOf("hd"))-1);
                    hf = t.substring(t.indexOf("hf") + 5, t.indexOf(",",t.indexOf("hf"))-1);
                    lastCoord = t.substring(t.indexOf("lastCoord")+12,t.indexOf("coord")-3);
                    coords = t.substring(t.indexOf("coords")+8,t.indexOf("}"));
                    Activity n = new Activity(user.getPseudo());
                    n.setType(type);
                    n.setHD(hd);
                    n.setHF(hf);
                    n.setlastCoord(lastCoord);
                    int fi=0,dep=0;
                    String si="\"",tmp2,tmp3="";
                    while ((fi = coords.indexOf(si, fi)) != -1) {
                        count++;
                        t = coords.substring(fi+1);
                        System.out.println(coords);
        
                        if(t.contains("\"")){
                            tmp2 = t.substring(0, t.indexOf("\""));
                            System.out.println("coordonnées recup :"+tmp2);
                            if(!tmp2.equals(",")){
                                System.out.println("true");
                                c.add(tmp2);
                            }
                        }
                        fi++;
                    }
                    n.setCoords(c); 
                    activites.add(n);
                }
            }
        }
        user.setActivites(activites);
        return user;
    }

    public static double calcDistance(String a, String b) {
        double lat1 = Double.parseDouble(a.substring(0, a.indexOf(",")));
        double lat2 = Double.parseDouble(b.substring(0, b.indexOf(",")));
        double lon1 = Double.parseDouble(a.substring(a.indexOf(",") + 1));
        double lon2 = Double.parseDouble(b.substring(b.indexOf(",") + 1));
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    public static void main(String[] args) throws IOException {
        Utilisateur test = getUser("test");
        Activity gn;
        double km = 0;
        System.out.println("<h1>Historique des activités</h1>");
        for (int i=0; i < test.getActivities().size(); i++){
            System.out.println("Type d'activité : " + test.getActivities().get(i).getType() + "<br> Coordonnées : <br>" + test.getActivities().get(i).getCoords() + "<br>Heure de début:" + test.getActivities().get(i).getHD() +"<br> Heure de fin :"+test.getActivities().get(i).getHF());
            for(int j=1; j < test.getActivities().get(i).getCoords().size();j++){
                km += calcDistance(test.getActivities().get(i).getCoords().get(j-1),test.getActivities().get(i).getCoords().get(j));
                System.out.println(km);
            }
            System.out.println("Nombre de km parcourus : " + km + "<br>");
            km =0;
        }
        // System.out.println("<h1>Dernière activité</h1>");
        //System.out.println("Type d'activité : " + gn.getType() + "<br> Coordonnées : <br>" + gn.getCoords() + "<br>Heure de début:" + gn.getHD() +"<br> Heure de fin :"+ gn.getHF());

    }
}
