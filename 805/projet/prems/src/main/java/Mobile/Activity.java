package Mobile;

import java.nio.channels.AcceptPendingException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Activity {

    listeActivites type;
    String pseudoUser;
    String hd;
    String hf;
    String lastCoord;
    ArrayList<String> coords;
    private static final SecureRandom random = new SecureRandom();

    public Activity(String pseudoUser){
        this.pseudoUser=pseudoUser;
        this.hd=addDeb();
        this.hf=hd;
        this.coords= new ArrayList<String>();
        addCordoonees();
        this.type=addNomActivite();
    }

    public void setHD(String hd){
        this.hd=hd;
    }

    public void setHF(String hf){
        this.hf=hf;
    }
    public void setlastCoord(String lc){
        this.lastCoord=lc;
    }
    public void setType(String type){
        this.type=listeActivites.valueOf(type);
    }
    private listeActivites addNomActivite() {
        return randomEnum(listeActivites.class);
    }

    public void setCoords(ArrayList<String> c){
        this.coords=c;
    }

    public String addDeb() {
        Random rd = new Random();
        return rd.nextInt(23) + ":" + rd.nextInt(59);
    }

    public String duree(){
        int Hdeb = Integer.parseInt(this.hd.substring(0, this.hd.indexOf(":", 0)));
        int Mdeb = Integer.parseInt(this.hd.substring(this.hd.indexOf(":", 0)+1));
        int Hfin = Integer.parseInt(this.hf.substring(0, this.hf.indexOf(":", 0)));
        int Mfin = Integer.parseInt(this.hf.substring(this.hf.indexOf(":", 0)+1));
        int resH = (Hfin - Hdeb)%24;
        int tmp=resH;
        if(resH < 0){
            tmp = 24 - resH;
        }
        int resM= Math.abs((Mfin - Mdeb)%60);
        return tmp + ":" + resM;
    }


    private void addFin(String heure) {
        String Hdeb = heure.substring(0, heure.indexOf(":", 0));
        String Mdeb = heure.substring(heure.indexOf(":", 0)+1);
        int hd = Integer.parseInt(Hdeb);
        int md = Integer.parseInt(Mdeb);
        int tmp;
        String hr="",hm="";
        md +=30;
        if (md >= 60){
            hd+=1;
            tmp=md%60;
            md=tmp;
        }
        tmp=hd%24;
        hd=tmp;
        if(hd < 10){
            hr+="0"+hd;
        }else{
           hr+=hd; 
        }
        if(md < 10){
            hm+="0"+md;
        }else{
           hm+=md; 
        }
        this.setHF(hr + ":" + hm);
    }

    public void addCordoonees() {
        Random rd = new Random();
        String lat,longueur,res = "";
        if(rd.nextInt(1)==1){
            lat=38+rd.nextInt(5)+"."+Math.abs(rd.nextInt());
        }
        else{
            lat=38-rd.nextInt(5)+"."+Math.abs(rd.nextInt());
        }
        if(rd.nextInt(1)==0){         
            longueur=3+ rd.nextInt(5)+"."+Math.abs(rd.nextInt());
        }
        else{      
            longueur=3- rd.nextInt(5)+"."+Math.abs(rd.nextInt());
        }
        res = lat+","+longueur;
        this.coords.add(res) ;
    }

    public String addNewCoords(ArrayList<String> co){
        String heure;
        String res;
        Random rd = new Random();
        String lastC = co.get(co.size()-1);
        String lat= lastC.substring(0,lastC.indexOf(','));
        String longueur=lastC.substring(lastC.indexOf(',')+1,lastC.length());
        String latr,longr =null;
        if(co.size()==1){
            heure = this.hd;
        }
        else{
            heure = this.hf;
        }
        if(rd.nextInt(2)==2){
            latr= Integer.parseInt(lat.substring(0,lat.indexOf('.'))) + rd.nextInt(1)+"."+Math.abs(rd.nextInt());
        }
        else{
            latr= Integer.parseInt(lat.substring(0,lat.indexOf('.'))) - rd.nextInt(1)+"."+Math.abs(rd.nextInt());
        }
        if(rd.nextInt(2)==2){         
            longr = Integer.parseInt(longueur.substring(0,longueur.indexOf('.'))) - rd.nextInt(1)+"."+Math.abs(rd.nextInt());
        }
        else{      
            longr = Integer.parseInt(longueur.substring(0,longueur.indexOf('.'))) + rd.nextInt(1)+"."+Math.abs(rd.nextInt());
        }
        res = latr +","+ longr;
        addFin(heure);
        this.coords.add(res);
        return res;
    }

    public static <T extends Enum<?>> T randomEnum(Class<T> clazz){
        int x = random.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    public ArrayList<String> getCoords(){
        return this.coords;
    }

    public String getLastCoord(){
        return this.coords.get(this.coords.size()-1);
    }

    public String getHD(){
        return this.hd;
    }

    public String getHF(){
        return this.hf;
    }

    public listeActivites getType(){
        return this.type;
    }

    public static void main(String[] args){
        Activity n= new Activity("test");
        System.out.println(n.getCoords());
        System.out.println(n.getHF());
        n.addNewCoords(n.getCoords());
        n.addNewCoords(n.getCoords());
        n.addNewCoords(n.getCoords());
        n.addNewCoords(n.getCoords());
        n.addNewCoords(n.getCoords());
        System.out.println(n.getCoords());
        System.out.println(n.getHF());
        System.out.println(n.duree());
    }
}